var express = require('express'),

	bodyParser = require('body-parser'),
	fs = require('fs'),
	http = require('http'),
	https = require('https'),
	morgan = require('morgan'),

	path = require('path'),
	config = require('./config/config'),
	crypto = require('crypto'),
	passport = require('passport'),
	glob = require("glob"),
	Fawn = require('./config/Fawn').Fawn,
	initPassport = require('./auth/initPassport'),
	jobs = require('./jobs/jobs');

// Main express App
var app = express();

/// Middleware goes here
app.use(require('cookie-parser')());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());

app.use("/campuspay/static",express.static(path.join(__dirname , "public")));
///////////  MOddlewared ends

initPassport.initPassport(app);


var mainRouter = express.Router();
app.use("/campuspay", mainRouter, function(req, res) {
	res.status(403).send("Bad request");
});

// Controllers must end with "Controller.js"
glob("./**/*Controller.js", function(err, files) {

	files.forEach(function(file) {
		var fileRoute = require(file);
		fileRoute.controller(mainRouter);
	});
});
// Controllers ends

// Launching transactional roll back Fawn 

process.on('uncaughtException', function(err) {
	console.log('**************************');
	console.log('* [process.on(uncaughtException)]: err:', err);
	console.log('**************************');
	process.exit(1);
});

var roller = Fawn.Roller();
roller.roll()
	.then(function() {
		console.log("Transactions rolled back..");
		app.set('port', process.env.PORT || 3040);
		http.createServer(app).listen(app.get('port'), function() {
			console.log('Express server listening on port ' + app.get('port'));
			jobs.launchJobs();
		});
	});

module.exports.app = app;
module.exports.Fawn = Fawn;