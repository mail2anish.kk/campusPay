var mongoose = require('../config/mongoose').Mongoose;
var Schema = mongoose.Schema;

var WalletSchema = new Schema({
	walletId: {
		type: String,
		required: [true, "affiliateId missing"],
	},
	userId: {
		type: String,
		required: [true, "user_id missing"],
		unique: [true, "duplicate user_id "]

	},
	balance: {
		type: Number,
		default: 0
	},
	rewards: {
		type: Number,
		default: 0
	},
	redeemed: {
		type: Number,
		default: 0
	},
	tentativePoints: {
		type: Number,
		default: 0
	},
	cashback: {
		type: Number,
		default: 0
	},
	created_on: {
		type: Date,
		default: Date.now
	},
	status: {
		type: Boolean,
		default: false
	},
	affiliates: Schema.Types.Mixed
});

module.exports = mongoose.model('Wallet', WalletSchema);