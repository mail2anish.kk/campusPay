var dbUtil = require('../config/dbUtil');

var tableName = "wallets";
module.exports.findOne = function(queryObj, options) {
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.findOne(queryObj, options, (err, wallet) => {
				if (err) {
					reject(err);
				} else {
					resolve(wallet);
				}
			});
		});
	});
};

module.exports.insert = function(walletObj) {
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.insertOne(walletObj, (err, wallet) => {
				if (err) {
					reject(err);
				} else {
					resolve(walletObj);
				}
			});
		});
	});
};

module.exports.update = function(queryObj, walletObj, options) {
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.update(queryObj, walletObj, options, (err, result) => {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
	});
};