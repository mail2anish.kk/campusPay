var bcrypt = require('bcryptjs');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');


const saltRounds = 10;
module.exports.encryptPassword = function(password) {
	return new Promise(function(resolve, reject) {
		bcrypt.genSalt(saltRounds, function(err, salt) {
			bcrypt.hash(password, salt, function(err, hash) {
				// Store hash in your password DB.
				if (err) {
					reject(err);
				} else {
					console.log("resoling promise");
					resolve({
						hash: hash,
						salt: salt
					});
				}
			});
		});
	});

}

module.exports.comparePasswords = function(password, hash) {
	return new Promise(function(resolve, reject) {
		bcrypt.compare(password, hash, function(err, res) {
			if (err || res == false) {
				reject(false);
			} else {
				resolve(true);
			}
		});
	});
};
module.exports.generateRandom = function(bits) {
	var buf = crypto.randomBytes(bits);
	return "TK" + buf.toString("hex").toUpperCase();
};

module.exports.signToken = function(tokenData, secret) {
	return jwt.sign(tokenData, secret);

};

module.exports.verifyToken = function(token, secret) {
	return jwt.verify(token, secret);
};

module.exports.decryptAES = function(key, value) {
	console.log("key:",key,"value is ",value);
	return new Promise((resolve, reject) => {
		try {
			var decipher = crypto.createDecipheriv('aes-128-cbc', key, key);
			decipher.setAutoPadding(true);
			var decrypted = decipher.update(value, 'base64', 'binary');

			decrypted += decipher.final('binary');

			resolve(decrypted.toString('utf8'));
		} catch (e) {
			console.log(e);
			reject(value);
		}

	});
}