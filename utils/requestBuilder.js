 var request = require('request-promise');
 var _ = require('lodash');



 module.exports.requestBuilder = function(reqInfo) {
 	var headers = {};
 	var body = {};
 	var paramsRet = {};
 	var URL = reqInfo.url;
 	var method = reqInfo.method;
 	var params = reqInfo.params;
 	if (params) {
 		var paramNames = Object.keys(reqInfo.params);
 		paramNames.forEach((key) => {
 			var param = params[key];
 			if (param.target == 'HEADER') {
 				_.set(headers, param.name, param.value);
 			} else {
 				_.set(body, param.name, param.value);
 			}
 		});
 		return {
 			headers: headers,
 			method: method,
 			uri: URL,
 			body: body
 		}
 	}
 };