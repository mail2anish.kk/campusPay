var passport = require('passport'),
    bcryptUtils = require('../utils/utils'),
    LocalStrategy = require('passport-local').Strategy,
    dbUtil = require('../config/dbUtil'),
    KryptoStrategy = require('./kryptostrategy');


module.exports.initPassport = function(app) {
    console.log("initialize pasport...");

    app.use(require('express-session')({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 3600000
        }
    }));


    passport.use('student', new KryptoStrategy());

    passport.use("local", new LocalStrategy(
        function(email, password, done) {
            dbUtil.getConnection(function(db) {
                var tableName = "users";
                db.collection(tableName).findOne({
                    email: email
                }, function(err, user) {
                    if (!user) {
                        return done(null, false, {
                            message: 'User not exist.'
                        });
                    } else {
                        bcryptUtils.comparePasswords(password, user.hash).then(function() {
                            return done(null, user);
                        }, function(err) {
                            return done(null, false, {
                                message: 'Password does not match.'
                            });
                        });

                    }
                });
            });

        }));

    passport.serializeUser(function(user, done) {
        //console.log("serializeUser called", user);

        user.salt = null;
        user.hash = null;
        done(null, user);
    });
    passport.deserializeUser(function(user, done) {
        //console.log("deserializeUser called", user);
        done(null, user);
    });

    app.use(passport.initialize());
    app.use(passport.session());

};

module.exports.passport =passport;