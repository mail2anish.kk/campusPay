var mongoose = require('mongoose');
var config = require('./config');

mongoose.createConnection(config.mongoUrl, {
		server: {
			poolSize: 10
		}
	},
	function(err, result) {
		if (err) {
			console.log("Some err ocuured while connecting to database .. ",err);
		} else {
			console.log("mongoose initialized");
		}
	});

module.exports.Mongoose = mongoose;