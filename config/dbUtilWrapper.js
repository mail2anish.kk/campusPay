var dbUtil = require('../config/dbUtil');


module.exports.save = function(collection, obj) {
	return new Promise(function(resolve, reject) {
		dbUtil.getConnection(function(db) {
			if (!db) {
				reject("Unable to connect to database .. ");
			}
			var collection = db[collection];
			collection.insertOne(obj, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
	});
};

module.exports.saveMulti = function(collection, obj) {
	return new Promise(function(resolve, reject) {
		if (!(obj instanceof Array)) {
			reject("Data must be an instance of array");
		}
		dbUtil.getConnection(function(db) {
			if (!db) {
				reject("Unable to connect to database .. ");
			}
			var collection = db[collection];
			collection.insert(obj, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});


		});
	});
};

module.exports.update = function(collection, queryObj, obj, options) {
	return new Promise(function(resolve, reject) {
		dbUtil.getConnection(function(db) {
			if (!db) {
				reject("Unable to connect to database .. ");
			}
			var collection = db[collection];
			collection.update(queryObj, {
				$set: obj
			}, options, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
	});
};

module.exports.findOne = function(collection, queryObj, options) {
	return new Promise(function(resolve, reject) {
		dbUtil.getConnection(function(db) {
			if (!db) {
				reject("Unable to connect to database .. ");
			}
			var collection = db[collection];
			collection.findOne(queryObj, options, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
	});
};
module.exports.find = function(collection, queryObj, options) {
return new Promise(function(resolve, reject) {

		dbUtil.getConnection(function(db) {
			if (!db) {
				reject("Unable to connect to database .. ");
			}
			var collection = db[collection];
			collection.find(queryObj, options).toArray(function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});


		});
	});
};