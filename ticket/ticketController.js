var express = require('express'),
	dbUtil = require('../config/dbUtil'),
	affSvc = require('../affiliates/affiliatesSvc'),
	ticketSvc = require('./ticketSvc'),
	ensureLoggedIn = require('../auth/ensureloggedin');

var ticketRouter = express.Router({
	mergeParams: true
});



module.exports.controller = function(app) {
	//offerRouter.use(ensureLoggedIn());

	app.use("/ticket", ticketRouter);

	ticketRouter.post("/save", function(req, res) {
		var ticket = req.body;
		ticketSvc.saveTicket(ticket).then((ticket) => {
			res.json(ticket);
		}, (err) => {
			console.log(err);
			res.status(503).send("Something went wrong !");
		});
	});

	ticketRouter.get("/get/all", function(req, res) {
		ticketSvc.findAll().then((tickets) => {
			res.json(tickets);
		}).catch((err) => {
			res.status(503).send("Something went wrong");
		});
	});
	ticketRouter.get("/get", function(req, res) {
		if (!req.query.ticketId) {
			res.status(403).send("Bad Request");
		}

		ticketSvc.findOne({
			ticketId: req.query.ticketId
		}).then((tickets) => {
			res.json(tickets);
		}).catch((err) => {
			res.status(503).send("Something went wrong");
		});

	});

};