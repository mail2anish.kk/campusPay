var dbUtil = require('../config/dbUtil');
var async = require('async');
var Ticket = require('./ticket');
var async = require("async");
var utils = require('../utils/utils');

var tableName = "tickets";
module.exports.updateTicket = function(queryObj, ticketObj, options) {
	options = options || {};

	return new Promise((resolve, reject) => {
		if (!ticketObj) {
			reject("updateObj must not be null");
		}
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.update(queryObj, {
				$set: ticketObj
			}, options, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

module.exports.saveTicket = function(ticketObj) {
	return new Promise((resolve, reject) => {
		if (!validateTicket(ticketObj)) {
			reject("Invalid Offer");
		}
		var ticket = new Ticket(ticketObj);
		async.retry(function(callback) {
			ticket.ticketId = utils.generateRandom(5);
			ticket.save((err, result) => {
				if (err) {
					callback(err);
				} else {
					callback(null, ticket);
				}
			});
		}, function(err, result) {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});

	});
};

module.exports.findAll = function(queryObj, options) {
	queryObj = queryObj || {};
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.find(queryObj, options).toArray((err, tickets) => {
				if (err) {
					reject(err);
				} else {
					resolve(tickets);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

module.exports.findOne = function(queryObj, options) {
	queryObj = queryObj || {};
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.findOne(queryObj, options, (err, ticket) => {
				if (err) {
					reject(err);
				} else {
					resolve(ticket);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

var validateTicket = function(ticketObj) {
	if (!ticketObj || !ticketObj.subject || !ticketObj.userid || !ticketObj.email || !ticketObj.message)
		return false;
	return true;
}