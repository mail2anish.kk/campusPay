var dateutil = require('dateutil');

var parseDate = function(str) {
	var dateTimeParts = str.split(" ");
	var dateParts = dateTimeParts[0].split("-");
	var timeParts = dateTimeParts[1].split(":")

	return new Date(parseInt(dateParts[2]), parseInt(dateParts[1]), parseInt(dateParts[0]), parseInt(timeParts[0]), parseInt(timeParts[1]), parseInt(timeParts[2]));
};

module.exports.convertFLIPKARTOrder = function(order, affiliateId, status) {
	
	if (!order)
		return order;
	var newOrder = {
		orderId: order.affiliateOrderItemId,
		productId: order.productId,
		orderDate: parseDate(order.orderDate),
		affiliateId: affiliateId,
		trk_id: order.affExtParam1,
		commission: order.tentativeCommission,
		source: order.salesChannel,
		title: order.title,
		category: order.category,
		price: order.price,
		status: status,
		ckStatus: "pending"
	};
	return newOrder;
};