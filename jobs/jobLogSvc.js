var dbUtil = require('../config/dbUtil');

module.exports.insertNewJob = function(jobData) {
	return new Promise((resolve, reject) => {
		dbUtil.getConnection((db) => {
			var jobColl = db.collection("jobslog");
			jobColl.insertOne(jobData, function(err, result) {
				if (err) {
					reject(err);
				} else {
					console.log("JOB data .. ", jobData);
					resolve(jobData);
				}
			});
		});
	});
};

module.exports.updateJob = function(queryObj, jobData, options) {
	options = options || {};
	return new Promise((resolve, reject) => {

		dbUtil.getConnection((db) => {
			var jobColl = db.collection("jobslog");
			jobColl.update(queryObj, {
				$set: jobData
			}, options, function(err, job) {
				if (err) {
					reject(err);
				} else {
					resolve(job);
				}
			});
		});
	});
};



module.exports.findJob = function(queryObj, options) {
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConnection((db) => {
			var jobColl = db.collection("jobslog");
			jobColl.findOne(queryObj, options, function(err, job) {
				if (err) {
					reject(err);
				} else {
					resolve(job);
				}
			});
		});
	});
};