var schedule = require('node-schedule');
var dbUtil = require('../config/dbUtil');
var affsSvc = require('../affiliates/affiliatesSvc');
var request = require('request-promise');
var jobSvc = require('./jobLogSvc');
var async = require('async');
var dateutil = require('dateutil');
var affiliatesOrderSvc = require("../affiliates_orders/orderSvc");
var walletSvc = require('../wallet/walletSvc');
var userSvc = require('../auth/userSvc');
var Fawn = require('../config/Fawn').Fawn;


var initCampusPayPointsJobs = function() {
	var jobName = "campus_pay_points";
	console.log("starting job", jobName);

	jobSvc.findJob({
		name: "fk_report_confirmed",
		status: "completed"
	}, {
		sort: [
			["created_on", -1]
		]
	}).then((job) => {
		if (!job) {
			return;

		}
		var endDate = job.end_date;
		var startDate = job.start_date;
		var jobObj = {
			start_date: startDate,
			end_date: endDate,
			name: jobName,
			status: "started",
			created_on: endDate
		};

		jobSvc.insertNewJob(jobObj).then((jobObj) => {
			affsSvc.getAffsConfigs().then((affs) => {
				var task = Fawn.Task();
				async.each(affs, (aff, callback) => {
					//console.log("Aff..", aff);
					var tableName = "affiliates_" + aff.affiliateId + "_orders";

					affiliatesOrderSvc.findOrders({
						"affiliateId": aff.affiliateId,
						"status": "approved",
						"ckStatus": "pending"
							//TODO ...
							/*"orderDate": {
								$gte: startDate,
								$lt: endDate
							}*/
					}).then((orders) => {
						console.log("Orders in wallet points..", orders.length, " for status approved",orders);
						if (orders.length == 0)
							callback();
						else {
							calculateOrderPoints(orders).then((calulatedPoints) => {
								async.each(calulatedPoints, (calculatedPoint, callback2) => {
									console.log("cpp",calulatedPoints);
									orders = calculatedPoint.orders;
									orders.forEach((order) => {
										task.update(tableName, {
											_id: order._id
										}, {
											$set: {
												"ckStatus": "done"
											}
										});
									});

									userSvc.findOne({
										trk_id: calculatedPoint.trk_id
									}).then((user) => {
										walletSvc.findOne({
											trk_id: calculatedPoint.trk_id
										}, {
											_id: 0
										}).then((wallet) => {

											saveOrUpdateWallet(task, wallet, user, aff, undefined, calculatedPoint.points);
											callback2();
										}).catch((err) => {
											throw err;
										});
									}).catch((err) => {
										// handle error
										throw err;
									});

								}, (err, result) => {
									if (!err) {
										console.log("savingg transactions.. for cnf points");
										task.run().then((results) => {
											//console.log("Result of task run..", results);
											callback();
										}).catch((err) => {
											console.log(err);
											throw err;
										});

									} else {
										throw err;
									}

								});
							}).catch((err) => {
								throw err;
							});
						}
					});
				}, (err, results) => {
					if (!err) {
						jobObj.status = "completed";
						jobSvc.updateJob({
							_id: jobObj._id
						}, jobObj).then((result) => {
							console.log("[ Job ][", jobObj.name, "] completed on", new Date());
						});
					} else {
						throw err;
					}
				});

			}).catch((err) => {
				throw err
			});
		}).catch((err) => {
			jobObj.status = "failed";
			jobSvc.updateJob({
				_id: jobObj._id
			}, jobObj).then((result) => {
				console.log("[ Job ][", jobObj.name, "] failed on", new Date());
			});
		});
	});
};



var calculateOrderPoints = function(orders) {
	return new Promise((resolve, reject) => {
		async.groupBy(orders, function(order, callback) {
			callback(null, order.trk_id);
		}, function(err, userOrders) {
			if (!err) {
				var calculatedPoints = [];
				Object.keys(userOrders).forEach((user_trk_id) => {
					var points = 0.0;
					ordersnow = userOrders[user_trk_id];
					ordersnow.forEach((ordernow) => {
						ordernow.ckStatus = "done";
						points += ordernow.commission.amount;
					});
					calculatedPoints.push({
						affiliateId: ordersnow[0].affiliateId,
						trk_id: user_trk_id,
						orders: ordersnow,
						points: points
					});
				});
				resolve(calculatedPoints);
			} else {
				reject(err);
			}
		});

	});
};

function saveOrUpdateWallet(task, wallet, user, aff, tentativePoints, cnfPoints) {
	if(!user){
		return;
	}
	tentativePoints = tentativePoints || 0;
	cnfPoints = cnfPoints || 0;
	var tentativeEarnings = tentativePoints * 0.4;
	var cnfEarnings = cnfPoints * 0.4;
	if (!wallet) {
		wallet = {
			userid: user.userid,
			trk_id: user.trk_id,
			balance: cnfEarnings,
			totalEarnings:cnfEarnings,
			cashback: 0,
			rewards: 0,
			redeemed: 0,
			tentativePoints: tentativeEarnings
		};
		wallet.affiliates = {};
		wallet.affiliates[aff.name] = {
			earning: cnfEarnings,
			tentativePoints: tentativeEarnings
		};

		task.save("wallets", wallet);
	} else {

		var incPath = "affiliates." + aff.name + ".earning";

		var tentativeIncPath = "affiliates." + aff.name + ".tentativePoints";

		var incPlaceHolder = {
			[incPath]: cnfEarnings,
			balance: cnfEarnings,
			totalEarnings : cnfEarnings,
			[tentativeIncPath]: tentativeEarnings,
			tentativePoints: tentativeEarnings

		};

		task.update("wallets", {
			trk_id: user.trk_id
		}, {
			"$inc": incPlaceHolder
		});
	}
}


var initCampusPayTentativePointsJobs = function() {
	var jobName = "campus_pay_tentative_points";
	console.log("starting job ", jobName);

	var current_date = new Date();
	var endDate = current_date;
	var startDate;
	jobSvc.findJob({
		name: jobName,
		status: "completed"
	}, {
		sort: [
			["created_on", -1]
		]
	}).then((job) => {
		if (!job) {
			startDate = new Date(endDate.getTime());
			startDate.setHours(0, 0, 0, 0);
			startDate.setDate(endDate.getDate() - 1);
		} else {
			startDate = job.end_date;
		}
		var jobObj = {
			start_date: startDate,
			end_date: endDate,
			name: jobName,
			status: "started",
			created_on: current_date
		};

		jobSvc.insertNewJob(jobObj).then((jobObj) => {
			affsSvc.getAffsConfigs().then((affs) => {
				var task = Fawn.Task();
				async.each(affs, (aff, callback) => {
					console.log("aff.name", aff);
					var tableName = "affiliates_" + aff.name + "_orders";

					affiliatesOrderSvc.findOrders({
						"affiliateId": aff.name,
						"status": "tentative",
						"ckStatus": "pending"
					}).then((orders) => {

						if (orders.length == 0) {
							callback();
						} else {
							calculateOrderPoints(orders).then((calulatedPoints) => {
								async.each(calulatedPoints, (calculatedPoint, callback2) => {
									calculatedPoint.orders.forEach((order) => {
										task.update(tableName, {
											_id: order._id
										}, {
											$set: {
												"ckStatus": "done"
											}
										});
									});
									userSvc.findOne({
										trk_id: calculatedPoint.trk_id
									}).then((user) => {
										walletSvc.findOne({
											trk_id: calculatedPoint.trk_id
										}, {
											_id: 0
										}).then((wallet) => {

											saveOrUpdateWallet(task, wallet, user, aff, calculatedPoint.points, undefined);

											callback2();
										}).catch((err) => {
											throw err;
										});
									}).catch((err) => {
										throw err;
									});

								}, (err, result) => {
									console.log("savingg transactions..");
									if (!err) {
										task.run().then((results) => {
											//console.log(results);
										}).catch((err) => {
											console.log(err);
										})
									} else {
										throw err;
									}
								});
							}).catch((err) => {
								throw err;
							});
						}
					});
				}, (err, results) => {
					if (!err) {
						jobObj.status = "completed";
						jobSvc.updateJob({
							_id: jobObj._id
						}, jobObj).then((result) => {
							console.log("[ Job ][", jobObj.name, "] completed on", new Date());
						});
					} else {
						throw err;
					}
				});

			}).catch((err) => {
				throw err
			});
		}).catch((err) => {
			jobObj.status = "failed";
			jobSvc.updateJob({
				_id: jobObj._id
			}, jobObj).then((result) => {
				console.log("[ Job ][", jobObj.name, "] failed on", new Date());
			});
		});
	});
};

module.exports.launchJobs = function() {
	schedule.scheduleJob('0 */10 * * *', initCampusPayPointsJobs());
	schedule.scheduleJob('0 */10 * * *', initCampusPayTentativePointsJobs());
};