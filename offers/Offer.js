var mongoose = require('../config/mongoose').Mongoose;

var Schema = mongoose.Schema;

var OfferSchema = new Schema({
	offerId: {
		type: String,
		unique: true
	},
	title: String,
	description: {
		type: String,
		required: [true, "description missing"]
	},
	affiliateId: {
		type: String,
		required: [true, "affiliate Id missing"]
	},
	offerType: {
		type: String,
		enum: ['COUPON', 'REFERRAL'],
		default: 'REFERRAL'
	},
	coupon: String,
	campaignURL: String,
	category: {
		id: String,
		name: String
	},
	created_on: {
		type: Date,
		default: Date.now
	},
	startDate: {
		type: Date,
		required: [true, "startDate is missing"]
	},
	endDate: {
		type: Date,
		required: [true, "startDate is missing"]
	},
	imgURL: String,
	status: {
		type: Boolean,
		default: true
	}
});

module.exports = mongoose.model('Offer', OfferSchema);