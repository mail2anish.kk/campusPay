var express = require('express'),
	dbUtil = require('../config/dbUtil'),
	userSvc = require('../auth/userSvc'),

	affSvc = require('../affiliates/affiliatesSvc'),
	offerSvc = require('./offerSvc'),
	ensureLoggedIn = require('../auth/ensureloggedin');

var offerRouter = express.Router({
	mergeParams: true
});

var defaultAffBuilder = function(userTrkId, url, aff) {
	if (!url.startsWith("http")) {
		url = "https://" + url
	}
	if (url.indexOf('?') == -1) {
		url += "?";
	}
	var trkParam = aff.trkParam;
	var userTrkParam = aff.userTrkParam;
	if (url.indexOf(trkParam.name) == -1) {
		url = url + "&" + trkParam.name + "=" + trkParam.value;
	}
	url = url + "&" + userTrkParam.name + "=" + userTrkId;
	return url;
};

module.exports.controller = function(app) {
	//offerRouter.use(ensureLoggedIn());

	app.use("/offer", offerRouter);

	offerRouter.get("/", (req, res) => {
		var aid = req.query.aid;
		var oid = req.query.oid;
		var userid = req.query.userid;

		if ((!aid || !oid) && userid) {
			res.status(403).send("Bad request");
		}
		if (aid) {
			offerSvc.getAffiliateOffers(aid).then((offers) => {
				res.json({
					type: "offers",
					data: offers
				});
			}).catch((err) => {
				res.status(503).send("Something went wrong");
			})
		}

		if (oid) {
			userSvc.findOne({
				userid: userid
			}).then((user) => {
				offerSvc.getOffer(oid).then((offer) => {
					affSvc.getAffsByName(offer.affiliateId).then((aff) => {

						var affBuilder;
						var launchURL;
						if (aff.affBuilder) {
							affBuilder = eval(aff.affBuilder);
							launchURL = offer.launchURL;
						} else {
							affBuilder = defaultAffBuilder;
							if(offer.launchURL){
								launchURL = offer.launchURL;
							}else{
								launchURL = aff.website;
							}
						}
						if (!user.trk_id) {
							response.status(501).send("Unsupported operations");
						}
						launchURL = affBuilder(user.trk_id, launchURL, aff);
						/*console.log("ddd", {
							type: "redirect",
							redirectURL: launchURL
						});*/
						res.json({
							type: "redirect",
							redirectURL: launchURL
						});

					}).catch((err) => {
						console.log("err", err);
						res.status(503).send("Something went wrong");
					});

				}).catch((err) => {

					console.log("err", err);
					res.status(503).send("Something went wrong");
				});
			}).catch((err) => {
				res.status(503).send(err);
			});

		}

	});

	offerRouter.get("/active", (req, res) => {
		offerSvc.getActiveOffers().then((offers) => {
			res.json(offers);
		}).catch((err) => {
			res.status(503).send("Something went wrong..");
		})
	});

	offerRouter.get("/aoffs", (req, res) => {
		offerSvc.getActiveForAffiliates().then((offers) => {
			res.json(offers);
		}).catch((err) => {
			console.log("err", err);
			res.status(503).send("Something went wrong..");
		})
	});

	offerRouter.get("/aff", (req, res) => {
		if (!req.query.affid) {
			res.status(403).send("Bad request");
		}
		affSvc.findOne({
			affiliateId: req.query.affid
		}).then((aff) => {
			res.json(aff);
		}).catch((err) => {
			console.log("err", err);
			res.status(503).send("Something went wrong..");
		})
	});

	offerRouter.post("/save", ensureLoggedIn(), (req, res) => {

		affSvc.findOne({
			affiliateId: req.body.affiliateId
		}).then((aff) => {
			var reqOffer = req.body;
			if (!reqOffer.imgURL) {
				reqOffer.imgURL = aff.logoURL;
			}
			offerSvc.saveOffer(reqOffer).then((offer) => {
				res.json(offer);
			}).catch((err) => {
				console.log("Err", err);
				res.status(503).send("Something went wrong..");
			})
		}).catch((err) => {
			console.log("Err", err);
			res.status(503).send("Something went wrong..");
		});

	});
};