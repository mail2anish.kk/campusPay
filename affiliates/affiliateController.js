var express = require('express'),
	dbUtil = require('../config/dbUtil'),
	offerSvc = require('../offers/offerSvc'),
	affiliateSvc = require('./affiliatesSvc'),
	ensureLoggedIn = require('../auth/ensureloggedin'),
	nodeStringify = require('node-stringify');


var affiliateRouter = express.Router({
	mergeParams: true
});


module.exports.controller = function(app) {
	console.log("Initializing affiliateController")
	affiliateRouter.use(ensureLoggedIn());
	affiliateRouter.use(function(req, res, next) {
		if (req.user && req.user.role && req.user.role == "ADMIN") {
			next();
		} else {
			res.status(401).json({
				status: "error",
				message: "Unauthenticated request"
			});

		}
	});
	app.use("/aff", affiliateRouter);


	affiliateRouter.get("/details", function(req, res) {
		affiliateSvc.getAffsConfigs().then((affs) => {
			res.json(affs);

		}).catch((err) => {
			res.status(503).send("Something went wrong");

		});
	});

	


	affiliateRouter.post("/save", function(req, res) {
		var affData = req.body;
		console.log("saving aff data .. ");
		affiliateSvc.saveAff(affData).then((aff) => {
			console.log("aff..", aff);
			res.json({
				status: "success",
				data: aff
			});
		}).catch((err) => {
			if (err.code == 11000) {
				res.status(503).send({
					"message": "Duplicate affiliate id",
					"status": "error"
				});
			} else {
				res.status(503).send({
					"status": "error",
					"message": "Something went wrong."
				});
			}
		});
	});

	affiliateRouter.post("/campaign/create", (req, res) => {
		offerSvc.saveOffer(req.body).then((result) => {
			res.json({
				"status": "success",
				"message": "campaign saved successfuly"
			});
		}).catch((err) => {
			res.status(403).send("Bad Request");
		})
	});
};