var dbUtil = require('../config/dbUtil');

module.exports.updateOrder = function(queryObj, orderObj, options) {
	options = options || {};

	return new Promise((resolve, reject) => {
		if (!orderObj) {
			reject("updateObj must not be null");
		}
		dbUtil.getConn().then((db) => {
			var tableName = "affiliates_" + queryObj.imade_affiliate_name + "_orders";
			var coll = db.collection(tableName);
			coll.update(queryObj, {
				$set: orderObj
			}, options, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};

module.exports.saveOrder = function(orderObj) {
	return new Promise((resolve, reject) => {
		if (!orderObj) {
			reject("updateObj must not be null");
		}
		dbUtil.getConn().then((db) => {
			var tableName = "affiliates_" + orderObj.imade_affiliate_name + "_orders";
			var coll = db.collection(tableName);
			coll.insertOne(orderObj, function(err, result) {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

module.exports.findOrders = function(queryObj, options) {
	options = options || {};

	return new Promise((resolve, reject) => {
		
		dbUtil.getConn().then((db) => {
			var tableName = "affiliates_" + queryObj.affiliateId + "_orders";
			console.log("Table in orderSvc .. ",tableName,"for query result is ..",queryObj);
			var coll = db.collection(tableName);
			coll.find(queryObj, options).toArray((err, result) => {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
};